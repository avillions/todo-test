build: docker composer migrations fixtures test get-token-root

docker:
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml -f docker-compose.override.yml build
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml -f docker-compose.override.yml up -d
	docker-compose ps

composer:
	docker-compose exec -T fpm bash -c 'composer install'

migrations:
	docker-compose exec -T fpm bash -c 'php bin/console doctrine:migrations:migrate --no-interaction'

fixtures:
	docker-compose exec -T fpm bash -c 'php bin/console doctrine:fixtures:load --no-interaction'

test:
	docker-compose exec -T fpm bash -c 'codecept run -vvv'

get-token-%:
	echo "Get root user token:"
	docker-compose exec -T fpm bash -c 'php bin/console lexik:jwt:generate-token $*'

attach-%:
	docker-compose exec $* bash
