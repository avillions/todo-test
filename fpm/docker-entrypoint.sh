#!/bin/bash
set -e

test -d /var/www/todo/var/logs/app || mkdir /var/www/todo/var/logs/app

if [ -d "/var/www/todo/app/config" ]; then
    cp /usr/local/share/parameters.yml /var/www/todo/app/config/parameters.yml
    cp /usr/local/share/.env /var/www/todo/.env
    chown -R www-data:www-data /var/www/todo
fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

exec "$@"
