<?php declare(strict_types = 1);

namespace ApiBundle\Manager;

use ApiBundle\Entity\User;
use ApiBundle\Entity\Task;
use ApiBundle\Entity\TaskData;
use ApiBundle\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Class TaskManager
 * @package ApiBundle\Manager
 */
class TaskManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TaskRepository|EntityRepository
     */
    private $repository;

    /**
     * TaskManager constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
        $this->repository = $em->getRepository(Task::class);
    }

    /**
     * @return TaskRepository
     */
    public function getRepository(): TaskRepository
    {
        return $this->repository;
    }

    /**
     * @param Task $task
     * @return self
     */
    public function flush(Task $task): self
    {
        $this->em->persist($task);
        $this->em->flush();

        return $this;
    }

    /**
     * @param TaskData $data
     * @param User     $user
     * @return Task
     */
    public function create(TaskData $data, User $user): Task
    {
        $task = (new Task())
            ->setContent($data->getContent())
            ->setCompleted($data->getCompleted())
            ->setUser($user)
        ;

        $this->flush($task);

        return $task;
    }

    /**
     * @param TaskData $data
     * @param User     $user
     * @return Task
     */
    public function update(TaskData $data, User $user): ?Task
    {
        /** @var Task $task */
        $criteria = ['id' => $data->getId(), 'user' => $user->getId()];
        if (!$task = $this->getRepository()->findOneBy($criteria)) {
            return null;
        }

        $task
            ->setContent($data->getContent())
            ->setCompleted($data->getCompleted())
            ->setUser($user)
        ;

        $this->flush($task);

        return $task;
    }

    /**
     * @param Task $task
     * @return self
     */
    public function delete(Task $task): self
    {
        $this->em->remove($task);
        $this->em->flush();

        return $this;
    }
}