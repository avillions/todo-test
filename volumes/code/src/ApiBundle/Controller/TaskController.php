<?php declare(strict_types = 1);

namespace ApiBundle\Controller;

use ApiBundle\Entity\ApiResponse;
use ApiBundle\Entity\TaskData;
use ApiBundle\Form\TaskType;
use ApiBundle\Manager\TaskManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ApiBundle\Entity\Task;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class TaskController
 * @package ApiBundle\Controller
 */
class TaskController extends AbstractController
{
    /**
     * List tasks.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return tasks.",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="success", type="boolean"),
     *         @SWG\Property(property="data", type="array", @SWG\Items(ref=@Model(type=Task::class, groups={"api"}))),
     *     )
     * )
     *
     * @SWG\Tag(name="task")
     *
     * @return Response
     * @throws HttpException
     */
    public function getAction()
    {
        if (!$result = $this->getDoctrine()->getRepository(Task::class)->findAll()) {
            throw new HttpException(
                Response::HTTP_NOT_FOUND,
                'There are no tasks exist'
            );
        }

        return $this->response(new ApiResponse($result));
    }

    /**
     * Create task.
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="json",
     *     description="Task data.",
     *     required=false,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="content", type="string"),
     *         @SWG\Property(property="completed", type="boolean"),
     *     )
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return created task.",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="success", type="boolean"),
     *         @SWG\Property(property="data", type="object", ref=@Model(type=Task::class, groups={"api"})),
     *     )
     * )
     *
     * @SWG\Tag(name="task")
     *
     * @param Request $request
     * @return Response
     */
    public function postAction(Request $request)
    {
        /** @var TaskData $data */
        $data = $this->getFormData(TaskType::class, $request);

        $post = $this->getManager()->create($data, $this->getUser());

        return $this->response(new ApiResponse($post, $request));
    }

    /**
     * Update task.
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="json",
     *     description="Task data.",
     *     required=false,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="content", type="string"),
     *         @SWG\Property(property="completed", type="boolean"),
     *     )
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return updated task.",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="success", type="boolean"),
     *         @SWG\Property(property="data", type="object", ref=@Model(type=Task::class, groups={"api"})),
     *     )
     * )
     *
     * @SWG\Tag(name="task")
     *
     * @param Request $request
     * @param int     $id
     * @return Response
     * @throws \HttpException
     */
    public function putAction(Request $request, int $id)
    {
        $data = $this->getFormData(TaskType::class, $request, ['id' => $id]);

        if (!$task = $this->getManager()->update($data, $this->getUser())) {
            throw new \HttpException(
                Response::HTTP_BAD_REQUEST,
                sprintf('Task #%s not found.', $id)
            );
        }

        return $this->response(new ApiResponse($task));
    }

    /**
     * Delete task.
     *
     * @SWG\Parameter(name="id", in="path", type="integer", description="Task id.")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return delete task result.",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="success", type="boolean"),
     *     )
     * )
     *
     * @SWG\Tag(name="task")
     *
     * @param int $id
     * @return null|object
     * @throws \HttpException
     */
    public function deleteAction(int $id)
    {
        $taskRepository = $this->getManager()->getRepository();

        /** @var Task $task */
        if (!$task = $taskRepository->find($id)) {
            throw new \HttpException(
                Response::HTTP_BAD_REQUEST,
                sprintf('Task #%s not found.', $id)
            );
        }

        $this->getManager()->delete($task);

        return $this->response(new ApiResponse());
    }

    /**
     * @return TaskManager
     */
    private function getManager(): TaskManager
    {
        return $this->get('api.manager.task');
    }
}
