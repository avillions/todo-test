<?php declare(strict_types=1);

namespace ApiBundle\DataFixtures\ORM;

use ApiBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadUserData
 * @package ApiBundle\DataFixtures\ORM
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $factory = $this->container->get('security.encoder_factory');
        $manager = $this->container->get('fos_user.user_manager');

        /** @var $user User */
        $user = $manager->createUser();
        $user->setUsername(User::DEFAULT_NAME);

        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword(User::DEFAULT_PASSWORD, $user->getSalt());

        $user->setPassword($password);

        $manager->updateUser($user);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder()
    {
        return 1;
    }
}