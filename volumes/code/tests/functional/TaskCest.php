<?php declare(strict_types = 1);

use Codeception\Util\HttpCode;
use ApiBundle\Entity\User;

/**
 * Class TaskCest
 */
class TaskCest
{
    const TASK_CONTENT = 'hello';

    const TASK_UPDATE_CONTENT = 'world';

    const TASK_COMPLETED = false;

    const TASK_UPDATE_COMPLETED = true;

    /**
     * @param FunctionalTester $I
     * @return null|int
     */
    public function post(FunctionalTester $I)
    {
        $I->amAuthenticate(User::DEFAULT_NAME, User::DEFAULT_PASSWORD);

        $url = $I->generateUrl('api.task.post');

        $I->sendPOST($url, [
            'content'   => self::TASK_CONTENT,
            'completed' => self::TASK_COMPLETED,
        ]);

        $I->seeResponseCodeIs(HttpCode::OK);

        $I->seeResponseIsJson();

        $I->seeResponseContainsJson([
            'success' => true,
            'data'    => [
                'content'   => self::TASK_CONTENT,
                'completed' => self::TASK_COMPLETED,
                'user'      => [
                    'username' => User::DEFAULT_NAME,
                ],
            ],
        ]);

        $I->canSeeInRepository(\ApiBundle\Entity\Task::class, [
            'content'   => self::TASK_CONTENT,
            'completed' => self::TASK_COMPLETED,
        ]);

        $jsonPath = '$.data.id';
        $I->seeResponseJsonMatchesJsonPath($jsonPath);
        $id = $I->grabDataFromResponseByJsonPath($jsonPath);

        return $id[0] ?? null;
    }

    /**
     * @param FunctionalTester $I
     */
    public function get(FunctionalTester $I)
    {
        $this->post($I);

        $I->amAuthenticate(User::DEFAULT_NAME, User::DEFAULT_PASSWORD);

        $url = $I->generateUrl('api.task.get');

        $I->sendGET($url);

        $I->seeResponseCodeIs(HttpCode::OK);

        $I->seeResponseIsJson();

        $I->seeResponseContainsJson([
            'success' => true,
            'data'    => [[]],
        ]);

        $jsonPath = '$.data..id';
        $I->seeResponseJsonMatchesJsonPath($jsonPath);
        $ids = $I->grabDataFromResponseByJsonPath($jsonPath);

        return $ids[array_rand($ids)];
    }

    /**
     * @param FunctionalTester $I
     * @param int|null         $taskId
     */
    public function put(FunctionalTester $I, int $taskId = null)
    {
        $I->amAuthenticate(User::DEFAULT_NAME, User::DEFAULT_PASSWORD);

        $taskId = $taskId ?? $this->post($I);

        $url = $I->generateUrl('api.task.update', ['id' => $taskId]);

        $I->sendPUT($url, [
            'content'   => self::TASK_UPDATE_CONTENT,
            'completed' => self::TASK_UPDATE_COMPLETED,
        ]);

        $I->seeResponseCodeIs(HttpCode::OK);

        $I->seeResponseIsJson();

        $I->seeResponseContainsJson([
            'success' => true,
            'data'    => [
                'content'   => self::TASK_UPDATE_CONTENT,
                'completed' => self::TASK_UPDATE_COMPLETED,
                'user'      => [
                    'username' => User::DEFAULT_NAME,
                ],
            ],
        ]);

        $I->canSeeInRepository(\ApiBundle\Entity\Task::class, [
            'content'   => self::TASK_UPDATE_CONTENT,
            'completed' => self::TASK_UPDATE_COMPLETED,
        ]);
    }

    /**
     * @param FunctionalTester $I
     */
    public function delete(FunctionalTester $I)
    {
        $I->amAuthenticate(User::DEFAULT_NAME, User::DEFAULT_PASSWORD);

        $taskId = $this->post($I);

        $url = $I->generateUrl('api.task.delete', ['id' => $taskId]);

        $I->sendDELETE($url);

        $I->seeResponseCodeIs(HttpCode::OK);

        $I->seeResponseIsJson();

        $I->dontSeeInRepository(\ApiBundle\Entity\Task::class, [
            'id'        => $taskId,
            'content'   => self::TASK_CONTENT,
            'completed' => self::TASK_COMPLETED,
        ]);
    }
}
