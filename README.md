# Todo test

## Running application
```bash
make 
```

## Or

### Step 1: Building Containers for Service
```bash
make docker
```

### Step 2: Install dependencies
```bash
make composer
```

### Step 3: Execute migrations
```bash
make migrations
```

### Step 4: Load fixtures
```bash
make fixtures
```

### Step 5: Run tests
```bash
make test
```

### Step 6: Get user JWT
```bash
make get-token-{username}
```



## Swagger documentation
[http://127.0.0.1:8888/swagger](http://127.0.0.1:8888/doc)
